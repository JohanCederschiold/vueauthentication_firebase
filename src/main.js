import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import * as firebase from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyCZ3La3F0EFk_lM4k9H3d8M4fNtlQT83jc",
  authDomain: "fir-demo-34eba.firebaseapp.com",
  databaseURL: "https://fir-demo-34eba.firebaseio.com",
  projectId: "fir-demo-34eba",
  storageBucket: "fir-demo-34eba.appspot.com",
  messagingSenderId: "511689297695",
  appId: "1:511689297695:web:2cf7179f6686bbce3b2a2a",
  measurementId: "G-TWJ5BJVKB7"
};

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(function(user) {
  console.log(store === undefined)
  if (user) {
    console.log('Logged in')
    store.commit('setUser', user.uid)
  } else {
    store.commit('setUser', null)
    console.log('Not logged in')
  }
});

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
