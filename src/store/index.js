import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userid: null,
    loggedin: false
  },
  mutations: {
    setUser(state, userid){
      console.log('Called', userid)
      if(userid !== null ) {
        state.userid = userid,
        state.loggedin = true
      } else {
        state.userid = null
        state.loggedin = false
      }
    }
  },
  actions: {
  },
  modules: {
  }
})
