import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/secretpage',
    name: 'Secretpage',
    component: () => import('../views/Secretpage.vue')
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) =>  {
  if(!(to.name === 'Login' || to.name === 'Register') && !store.state.loggedin) {
    next({name: 'Login'})
  } else {
    next()
  }
})

export default router
